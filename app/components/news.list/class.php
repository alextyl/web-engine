<?php


class NewsList extends Component
{

    protected function prepareParams()
    {
        if (empty($this->params["newsOnPageCount"])) // количество новостей на одной странице
        {
            $this->params["newsOnPageCount"] = 3;
        }

        if (empty($this->params["rightLeftNews"])) // количество страниц справа и слева от текущей
        {
            $this->params["rightLeftNews"] = 3;
        }

        if (empty($this->params["newsTable"])) // имя таблицы с новосятми
        {
            $this->params["newsTable"] = "news";
        }
    }

    public function executeComponent()
    {
        $this->prepareParams();
        $this->result["paginationExtraInfo"] = $this->getPaginationExtraInfo();
        $this->result["news"] = $this->getNews($this->result["paginationExtraInfo"]["currentStartNew"], $this->params["newsOnPageCount"]);
        $this->includeTemplate();
    }

    private function getPaginationExtraInfo()
    {
        $paginationExtraInfo["newsСount"] = 36; // количество новостей в базе
        $paginationExtraInfo["currentPage"] = $_GET["page"]; // текущая страница
        $paginationExtraInfo["pageCount"] = intval((($paginationExtraInfo["newsСount"] - 1) / $this->params["newsOnPageCount"]) + 1); // количество страниц
        if (empty($paginationExtraInfo["currentPage"]) or $paginationExtraInfo["currentPage"] < 0) {
            $paginationExtraInfo["currentPage"] = 1;
        }
        if ($paginationExtraInfo["currentPage"] > $paginationExtraInfo["pageCount"]) {
            $paginationExtraInfo["currentPage"] = $paginationExtraInfo["pageCount"];
        }
        $paginationExtraInfo["currentStartNew"] = $this->params["newsOnPageCount"] * $paginationExtraInfo["currentPage"] - $this->params["newsOnPageCount"]; // с какой новости следует выводить блок
        $paginationExtraInfo["leftPageCount"] = $paginationExtraInfo["currentPage"] - 1; // количество страниц левее текущей в пагинации
        if ($paginationExtraInfo["leftPageCount"] > $this->params["rightLeftNews"]) {
            $paginationExtraInfo["leftPageCount"] = $this->params["rightLeftNews"];
        }

        $paginationExtraInfo["rightPageCount"] = $paginationExtraInfo["pageCount"] - $paginationExtraInfo["currentPage"]; // количество страниц правее текущей в пагинации
        if ($paginationExtraInfo["rightPageCount"] > $this->params["rightLeftNews"]) {
            $paginationExtraInfo["rightPageCount"] = $this->params["rightLeftNews"];
        }
        return $paginationExtraInfo;
    }

    private function getNews($start, $count) // переписать в следующем таске для работы с БД
    {
        $needNews = array();

        $db = MySqlDB::getInstance();
        $news = $db->select($this->params["newsTable"], "1 = 1 LIMIT $start, $count");

        for ($i = 0; $i < $count; $i++) {
            $new = mysqli_fetch_assoc($news);
            $needNews[] = $new;
        }

        return $needNews;
    }
}

?>