<?php

const TEMPLATE = "
                    <div class=\"new\">
                        <h2>#TITLE#</h2>
                        <div class='description'>#DESCRIPTION#</div>
                        <br/>
                        <a class='link' href=\"#LINKMORE#\" target=\"_blank\">Подробнее</a>
                    </div>
                    ";

$html = "";
$macros = "";
for ($i = 0; $i < count($arrResult["news"]); $i++) {
    $macros = Component::getMacros($arrResult["news"][$i]);
    $html .= str_replace($macros, $arrResult["news"][$i], TEMPLATE);
}



echo $html;


if ($arrResult["paginationExtraInfo"]["leftPageCount"] == 1) {
    echo '<a href=index.php?page=' . 1 . ' id=link_' . 1 . ' name="pagination_link">' . " " . 1 . " " . '</a>';
}

if ($arrResult["paginationExtraInfo"]["leftPageCount"] == 2) {
    echo '<a href=index.php?page=' . 1 . ' id=link_' . 1 . ' name="pagination_link">' . " " . 1 . " " . '</a>';
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["currentPage"] - 1) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["currentPage"] - 1) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["currentPage"] - 1) . " " . '</a>';
}

if ($arrResult["paginationExtraInfo"]["leftPageCount"] == 3) {
    echo '<a href=index.php?page=' . 1 . ' id=link_' . 1 . ' name="pagination_link">' . " " . 1 . " " . ' </a>';
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["currentPage"] / 2) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["currentPage"] / 2) . ' name="pagination_link">' . " " . ' ... ' . " " . '</a>';
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["currentPage"] - 1) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["currentPage"] - 1) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["currentPage"] - 1) . " " . '</a>';
}



echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["currentPage"]) . ' id=link_currentPage name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["currentPage"]) . " " . '</a>';

if ($arrResult["paginationExtraInfo"]["rightPageCount"] == 1) {
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["pageCount"]) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["pageCount"]) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["pageCount"]) . " " . '</a>';
}

if ($arrResult["paginationExtraInfo"]["rightPageCount"] == 2) {
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["currentPage"] + 1) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["currentPage"] + 1) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["currentPage"] + 1) . " " . '</a>';
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["pageCount"]) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["pageCount"]) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["pageCount"]) . " " . '</a>';
}

if ($arrResult["paginationExtraInfo"]["rightPageCount"] == 3) {
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["currentPage"] + 1) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["currentPage"] + 1) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["currentPage"] + 1) . " " . '</a>';
    echo '<a href=index.php?page=' . intval(($arrResult["paginationExtraInfo"]["currentPage"] + ($arrResult["paginationExtraInfo"]["pageCount"] - $arrResult["paginationExtraInfo"]["currentPage"]) / 2) + 1) . ' id=link_' . intval(($arrResult["paginationExtraInfo"]["currentPage"] + ($arrResult["paginationExtraInfo"]["pageCount"] - $arrResult["paginationExtraInfo"]["currentPage"]) / 2) + 1) . ' name="pagination_link">' . " " . ' ... ' . " " . '</a>';
    echo '<a href=index.php?page=' . intval($arrResult["paginationExtraInfo"]["pageCount"]) . ' id=link_' . intval($arrResult["paginationExtraInfo"]["pageCount"]) . ' name="pagination_link">' . " " . intval($arrResult["paginationExtraInfo"]["pageCount"]) . " " . '</a>';
}



?>
