<?php

final class Application
{


    private static $instance;
    private $template = "";
    private $property = array();
    private $__components = array();

    private function __construct()
    {
        self::includeFile("app/init.php");
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        return self::$instance == null ? self::$instance = new static() : self::$instance;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getTemplatePath()
    {
        return "app/templates/" . $this->template;
    }

    public static function includeFile($file)
    {
        $file = $_SERVER['DOCUMENT_ROOT'] . '/' . $file;
        if (is_file($file)) {
            include $file;
            return true;
        }
        return false;
    }

    public function showHeader()
    {
        $this->handler("onEpilog");
        self::includeFile($this->getTemplatePath() . '/header.php');
    }

    public function showFooter()
    {
        self::includeFile($this->getTemplatePath() . '/footer.php');
        $this->handler("onProlog");
    }

    public function handler($event)
    {
        if (function_exists($event)) {
            call_user_func($event);
            return true;
        }
        return false;
    }

    public function includeComponent($name, $template, $params)
    {
        if(empty($this->__components[$name]))
        {
            self::includeFile("app/components/" . $name . "/" . "class.php");
            $this->__components[$name] = $this->findingHeir(get_declared_classes());
            $component = new $this->__components[$name]($name, $template, $params);
            $component->executeComponent();
        }
    }

    private function findingHeir($class_array)
    {
        for ($i = count($class_array); $i >= 0; $i--)
        {
            if(is_subclass_of($class_array[$i], 'Component'))
            {
                return $class_array[$i];
            }

        }
        return null;
    }

    public function setPageProperty($id, $value)
    {
        $this->property[$id] = $value;
    }

    public function getPageProperty($id)
    {
        return $this->property[$id];
    }

    public function showPageProperty($id)
    {
        echo $this->getPageProperty($id);
    }


    public function restartBuffer()
    {
        ob_clean();
    }

    public static function showVarible($var)
    {
        echo '<pre>' . print_r($var, true) . '</pre>';
    }

}

abstract class Component
{

    protected $name = "";
    protected $template = "";
    protected $params = array();
    protected $result = array();

    public function __construct($name, $template, $params)
    {
        $this->name = $name;
        $this->template = $template;
        $this->params = $params;
    }

    abstract protected function prepareParams();

    abstract protected function executeComponent();

    public static function getMacros($array)
    {
        $macros = array();
        $keys = array_keys($array);
        foreach ($keys as $key) {
            $macros[] = "#" . strtoupper($key) . "#";
        }
        return $macros;
    }

    final protected function includeTemplate($file = "template")
    {
        $arrParams = $this->params;
        $arrResult = $this->result;
        include("app/components/" . $this->name . "/" . $this->template . "/" . $file . ".php");
    }

}

final class MySqlDB
{

    private static $instance;
    private $link = "";

    private function __construct()
    {
        include("config.php");
        $this->link = mysqli_connect($config["DBInfo"]["HOST"], $config["DBInfo"]["USER"], $config["DBInfo"]["PASSWORD"], $config["DBInfo"]["DATABASE"])
        or die("Ошибка подключения к базе данных: " . mysqli_error($this->link));
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        return self::$instance == null ? self::$instance = new static() : self::$instance;
    }

    public function select($table, $condition = "1 = 1")
    {
        $sqlQuery = "SELECT * FROM " . $this->link->real_escape_string($table). " WHERE " . $this->link->real_escape_string($condition);
        return mysqli_query($this->link, $sqlQuery);
    }

    public function update($table, $setInfo, $condition = "1 = 1")
    {
        $sqlQuery = "UPDATE " . $this->link->real_escape_string($table). " SET " . $this->link->real_escape_string($setInfo). " WHERE " . $this->link->real_escape_string($condition);
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function delete($table, $condition = "1 = 1")
    {
        $sqlQuery = "DELETE FROM " . $this->link->real_escape_string($table). " WHERE " . $this->link->real_escape_string($condition);
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function insert($table, $values)
    {
        $sqlQuery = "INSERT INTO " . $this->link->real_escape_string($table). " VALUES (" . $this->link->real_escape_string($table). ")";
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function query($sqlQuery)
    {
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function tableList($db)
    {
        $sqlQuery = "SHOW TABLES FROM " . $this->link->real_escape_string($db);
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function __destruct()
    {
        mysqli_close($this->link);
    }

}

?>